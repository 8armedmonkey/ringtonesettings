package com.eightam.android.ringtonesettings.util;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import java.util.List;

public class RingtoneUtils {

    public static Ringtone getRingtoneById(Context context, Integer ringtoneId) {
        Uri ringtoneUri = getRingtoneBaseUri(context).buildUpon().appendPath(String.valueOf(ringtoneId)).build();
        return RingtoneManager.getRingtone(context, ringtoneUri);
    }

    public static void playRingtoneById(Context context, Integer ringtoneId) {
        Ringtone ringtone = getRingtoneById(context, ringtoneId);
        ringtone.play();
    }

    public static Uri getRingtoneBaseUri(Context context) {
        Uri actualDefaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_NOTIFICATION);
        List<String> pathSegments = actualDefaultRingtoneUri.getPathSegments();

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(actualDefaultRingtoneUri.getScheme());
        builder.authority(actualDefaultRingtoneUri.getAuthority());

        for (int i = 0, n = pathSegments.size() - 1; i < n; i++) {
            String pathSegment = pathSegments.get(i);
            builder.appendPath(pathSegment);
        }

        return builder.build();
    }

}
