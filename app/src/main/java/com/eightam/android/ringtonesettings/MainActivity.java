package com.eightam.android.ringtonesettings;

import android.media.Ringtone;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.eightam.android.ringtonesettings.fragment.RingtoneSelectionDialogFragment;
import com.eightam.android.ringtonesettings.storage.RingtoneSettingsStorage;
import com.eightam.android.ringtonesettings.util.RingtoneUtils;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener, RingtoneSelectionDialogFragment.Listener {

    private static final String FRAGMENT_RINGTONE_SELECTION = "ringtoneSelection";

    private RingtoneSettingsStorage mRingtoneSettingsStorage;

    private TextView mTextRingtone;

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);

        if (fragment instanceof RingtoneSelectionDialogFragment) {
            ((RingtoneSelectionDialogFragment) fragment).setListener(MainActivity.this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_select: {
                showRingtoneSelectionDialog();
                break;
            }

            case R.id.button_play: {
                playSelectedRingtone();
                break;
            }
        }
    }

    @Override
    public void onRingtoneSaved(Integer ringtoneId) {
        updateUi();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();

        mTextRingtone = (TextView) findViewById(R.id.text_ringtone);

        findViewById(R.id.button_select).setOnClickListener(MainActivity.this);
        findViewById(R.id.button_play).setOnClickListener(MainActivity.this);

        updateUi();
    }

    private void initialize() {
        mRingtoneSettingsStorage = new RingtoneSettingsStorage(MainActivity.this);
    }

    private void showRingtoneSelectionDialog() {
        RingtoneSelectionDialogFragment ringtoneSelectionDialogFragment =
                (RingtoneSelectionDialogFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_RINGTONE_SELECTION);

        if (ringtoneSelectionDialogFragment == null) {
            ringtoneSelectionDialogFragment = RingtoneSelectionDialogFragment.newInstance(MainActivity.this);
            ringtoneSelectionDialogFragment.show(getSupportFragmentManager(), FRAGMENT_RINGTONE_SELECTION);
        }
    }

    private void playSelectedRingtone() {
        Integer ringtoneId = mRingtoneSettingsStorage.getRingtoneId();
        RingtoneUtils.playRingtoneById(MainActivity.this, ringtoneId);
    }

    private void updateUi() {
        Integer ringtoneId = mRingtoneSettingsStorage.getRingtoneId();
        Ringtone ringtone = RingtoneUtils.getRingtoneById(MainActivity.this, ringtoneId);

        mTextRingtone.setText(ringtone.getTitle(MainActivity.this));
    }

}
