package com.eightam.android.ringtonesettings.widget;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.eightam.android.ringtonesettings.R;
import com.eightam.android.ringtonesettings.util.RingtoneUtils;

public class RingtoneAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private RingtoneManager mRingtoneManager;
    private Uri mRingtoneBaseUri;
    private Integer mSelectedRingtoneId;

    public RingtoneAdapter(Context context) {
        this(context, 0);
    }

    public RingtoneAdapter(Context context, Integer selectedRingtoneId) {
        mContext = context;
        mSelectedRingtoneId = selectedRingtoneId;

        initializeRingtoneManager();
        initializeRingtoneBaseUri();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_ringtone, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Uri ringtoneUri = mRingtoneManager.getRingtoneUri(position);
        Uri selectedRingtoneUri = mRingtoneBaseUri.buildUpon().appendPath(String.valueOf(mSelectedRingtoneId)).build();

        Ringtone ringtone = RingtoneManager.getRingtone(mContext, ringtoneUri);
        Integer ringtoneId = Integer.parseInt(ringtoneUri.getLastPathSegment());
        boolean isSelected = ringtoneUri.equals(selectedRingtoneUri);

        ((ItemViewHolder) holder).mTextTitle.setText(ringtone.getTitle(mContext));
        ((ItemViewHolder) holder).mRadioSelectRingtone.setChecked(isSelected);
        ((ItemViewHolder) holder).mRadioSelectRingtone.setOnClickListener(new SelectRingtoneClickListener(ringtoneId));
    }

    @Override
    public int getItemCount() {
        return mRingtoneManager.getCursor().getCount();
    }

    public Integer getSelectedRingtoneId() {
        return mSelectedRingtoneId;
    }

    private void initializeRingtoneManager() {
        mRingtoneManager = new RingtoneManager(mContext);
        mRingtoneManager.setType(RingtoneManager.TYPE_NOTIFICATION);
    }

    private void initializeRingtoneBaseUri() {
        mRingtoneBaseUri = RingtoneUtils.getRingtoneBaseUri(mContext);
    }

    public void selectAndPlayRingtone(Integer ringtoneId) {
        synchronized (RingtoneAdapter.this) {
            selectRingtone(ringtoneId);
            playRingtone(ringtoneId);
        }
    }

    public void selectRingtone(Integer ringtoneId) {
        synchronized (RingtoneAdapter.this) {
            mSelectedRingtoneId = ringtoneId;
        }
    }

    public void playRingtone(Integer ringtoneId) {
        synchronized (RingtoneAdapter.this) {
            Uri ringtoneUri = mRingtoneBaseUri.buildUpon().appendPath(String.valueOf(ringtoneId)).build();
            Ringtone ringtone = RingtoneManager.getRingtone(mContext, ringtoneUri);

            ringtone.play();
        }
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView mTextTitle;
        RadioButton mRadioSelectRingtone;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mTextTitle = (TextView) itemView.findViewById(R.id.text_title);
            mRadioSelectRingtone = (RadioButton) itemView.findViewById(R.id.radio_select_ringtone);
        }

    }

    class SelectRingtoneClickListener implements View.OnClickListener {

        Integer mRingtoneId;

        public SelectRingtoneClickListener(Integer ringtoneId) {
            mRingtoneId = ringtoneId;
        }

        @Override
        public void onClick(View v) {
            selectAndPlayRingtone(mRingtoneId);
        }

    }

}
