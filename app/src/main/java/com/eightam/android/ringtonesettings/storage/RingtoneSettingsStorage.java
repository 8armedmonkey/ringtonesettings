package com.eightam.android.ringtonesettings.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;

public class RingtoneSettingsStorage {

    private static final String PREFS_NAME = "ringtoneSettings";
    private static final String KEY_RINGTONE_ID = "ringtoneId";

    private Context mAppContext;
    private SharedPreferences mSharedPreferences;

    public RingtoneSettingsStorage(Context context) {
        mAppContext = context.getApplicationContext();
        mSharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public Integer getRingtoneId() {
        Uri uri = RingtoneManager.getActualDefaultRingtoneUri(mAppContext, RingtoneManager.TYPE_NOTIFICATION);
        int ringtoneId = Integer.parseInt(uri.getLastPathSegment());

        return mSharedPreferences.getInt(KEY_RINGTONE_ID, ringtoneId);
    }

    public void setRingtoneId(Integer ringtoneId) {
        mSharedPreferences.edit().putInt(KEY_RINGTONE_ID, ringtoneId).apply();
    }

}
