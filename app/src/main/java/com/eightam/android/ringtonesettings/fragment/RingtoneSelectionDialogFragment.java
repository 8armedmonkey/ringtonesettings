package com.eightam.android.ringtonesettings.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;

import com.eightam.android.ringtonesettings.R;
import com.eightam.android.ringtonesettings.storage.RingtoneSettingsStorage;
import com.eightam.android.ringtonesettings.widget.RingtoneAdapter;

import java.lang.ref.SoftReference;

public class RingtoneSelectionDialogFragment extends DialogFragment implements View.OnClickListener {

    private RingtoneSettingsStorage mRingtoneSettingsStorage;
    private SoftReference<Listener> mListenerRef;

    public static RingtoneSelectionDialogFragment newInstance(Listener listener) {
        RingtoneSelectionDialogFragment fragment = new RingtoneSelectionDialogFragment();
        fragment.mListenerRef = new SoftReference<>(listener);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_ringtone_selection);

        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new RingtoneAdapter(getActivity()));

        dialog.findViewById(R.id.button_save).setOnClickListener(RingtoneSelectionDialogFragment.this);
        dialog.findViewById(R.id.button_cancel).setOnClickListener(RingtoneSelectionDialogFragment.this);

        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_save: {
                RecyclerView recyclerView = (RecyclerView) getDialog().findViewById(R.id.recycler_view);
                Integer selectedRingtoneId = ((RingtoneAdapter) recyclerView.getAdapter()).getSelectedRingtoneId();

                saveRingtone(selectedRingtoneId);
                break;
            }
        }

        dismiss();
    }

    public void setListener(Listener listener) {
        mListenerRef = new SoftReference<>(listener);
    }

    private void initialize() {
        mRingtoneSettingsStorage = new RingtoneSettingsStorage(getActivity());
    }

    private void saveRingtone(Integer ringtoneId) {
        mRingtoneSettingsStorage.setRingtoneId(ringtoneId);
        notifyListenerOnRingtoneSaved(ringtoneId);
    }

    private void notifyListenerOnRingtoneSaved(Integer ringtoneId) {
        Listener listener = mListenerRef.get();

        if (listener != null) {
            listener.onRingtoneSaved(ringtoneId);
        }
    }

    public interface Listener {

        void onRingtoneSaved(Integer ringtoneId);

    }

}
